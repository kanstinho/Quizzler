//
//  FlipObjectView.swift
//  Quizzler
//
//  Created by Leon Kanstinger on 15.09.23.
//
import SwiftUI

struct FlipObjectView: View {
    var cardTextFront: String
    var cardTextBack: String
    var withImage: Bool
    @State private var degrees = 0.0
    @State private var isFlipped = false
    @State private var whileFlipping = ""

    var body: some View {
        VStack {
            ZStack {
                CardViewText(content: isFlipped ? whileFlipping : cardTextFront, withImage: withImage)
                    .rotation3DEffect(
                        .degrees(degrees),
                        axis: (x: 0, y: 1, z: 0),
                        anchor: .center,
                        anchorZ: 0.0,
                        perspective: 0.5
                    )
                    .zIndex(isFlipped ? 0 : 1)

                CardViewText(content: isFlipped ? cardTextBack : whileFlipping, withImage: false)
                    .rotation3DEffect(
                        .degrees(180 + degrees),
                        axis: (x: 0, y: 1, z: 0),
                        anchor: .center,
                        anchorZ: 0.0,
                        perspective: 0.5
                    )
                    .zIndex(isFlipped ? 1 : 0)
            }
            .transition(.scale)
            .onTapGesture {
                withAnimation(.easeInOut(duration: 0.5)) {
                    self.degrees += 180
                    self.isFlipped.toggle()
                }
            }
        }
    }
}

struct CardViewText: View {
    let content: String
    let withImage: Bool

    var body: some View {
        if withImage == false {
            RoundedRectangle(cornerRadius: 10)
                .fill(Color.gray)
                .overlay(
                    Text(content)
                        .font(.largeTitle)
                        .foregroundColor(.white)
                )
        } else {
            RoundedRectangle(cornerRadius: 10)
                .fill(Color.gray)
                .overlay(
                    Image(content)
                        .resizable()
                        .scaledToFit()
                        .clipShape(RoundedRectangle(cornerRadius: 10))
                        .aspectRatio(contentMode: .fit)
                )
        }
    }
}
