//
//  QuestionViewV2.swift
//  Quizzler
//
//  Created by Leon Kanstinger on 15.09.23.
//

import SwiftUI

struct QuestionViewV2: View {
    let players: [Player] = [
        Player(initial: "A", symbol: "⭐️", score: 0),
        Player(initial: "B", symbol: "🌟", score: 0),
        // Fügen Sie hier weitere Spieler hinzu, je nach Auswahl
    ]

    @State private var activePlayerIndex = 0

    var body: some View {
        VStack {
            HStack(spacing: 10) {
                ForEach(players.indices, id: \.self) { index in
                    let player = players[index]
                    let isActive = index == activePlayerIndex

                    IcicleView(player: player, isActive: isActive)
                }
                HStack {
                    FlipObjectView(cardTextFront: "Frosch", cardTextBack: "answer", withImage: true)
                    FlipObjectView(cardTextFront: "question", cardTextBack: "answer", withImage: false)
                }
                HStack {
                    FlipObjectView(cardTextFront: "Frosch", cardTextBack: "answer", withImage: true)
                    FlipObjectView(cardTextFront: "question", cardTextBack: "answer", withImage: false)
                }
                HStack {
                    FlipObjectView(cardTextFront: "Frosch", cardTextBack: "answer", withImage: true)
                    FlipObjectView(cardTextFront: "question", cardTextBack: "answer", withImage: false)
                }
                HStack {
                    FlipObjectView(cardTextFront: "Frosch", cardTextBack: "answer", withImage: true)
                    FlipObjectView(cardTextFront: "question", cardTextBack: "answer", withImage: false)
                }
                HStack {
                    FlipObjectView(cardTextFront: "Frosch", cardTextBack: "answer", withImage: true)
                    FlipObjectView(cardTextFront: "question", cardTextBack: "answer", withImage: false)
                }
            }
        }
    }
}
