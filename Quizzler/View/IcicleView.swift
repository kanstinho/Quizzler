//
//  IcicleView.swift
//  Quizzler
//
//  Created by Leon Kanstinger on 25.10.23.
//

import SwiftUI

struct IcicleView: View {
    let player: Player
    let isActive: Bool
    
    var body: some View {
        VStack {
            Text(player.initial)
                .font(.title)
            
            Text(player.symbol)
                .font(.title)
            
            Text("Score: \(player.score)")
                .font(.caption)
        }
        .padding(10)
        .background(isActive ? Color.blue : Color.gray)
        .cornerRadius(10)
    }
}

struct Player {
    let initial: String
    let symbol: String
    var score: Int
}
