//
//  Info.swift
//  Quizzler
//
//  Created by Ejona Syla on 13.09.23.
//

import SwiftUI

struct Info: Codable{
    var title: String
    var peopleAttended: Int
    var rules: [String]

    enum CodingKeys: CodingKey {
        case title
        case peopleAttended
        case rules
    }
}
