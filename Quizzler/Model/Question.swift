//
//  Question.swift
//  Quizzler
//
//  Created by Ejona Syla on 13.09.23.
//

import SwiftUI

struct Question: Identifiable, Codable {
    var id: UUID = .init()
    var question: String
    var options: [String]
    var answer: String

    var tappedAnswer: String = ""

    enum CodingKeys: CodingKey {
        case question
        case options
        case answer
    }
}
